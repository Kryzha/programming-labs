#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/** Константа, котоаря обозначает точность расчёта квадратного корня*/
#define ACCURACY 0.001
/** Константа, которая обозначает коичество строк и столбцов в квадратной матрице*/
#define SIZE_OF_ROWS_AND_COLUMNS 3

/**
 * Модуль числа
 * @param number число, с которого нужно посчитать модуль
 * @return модуль переданного числа в функцию
 */
double module(double number);

/**
 * Квадрат числа
 * @param number число, которое нужно вознести в квадрат
 * @return квадрат переданного числа в функцию
 */
double sqr(double number);

/**
 * Корень числа
 * @param number число, с которого нужно подсчитать корень
 * @return корень переданного числа в функцию
 */
int sqrt_from_number(int given_number);

/**
 * Умножение матрицы саму на себя
 * @param matrix_string матрица, которая переобразована в одинарный массив
 * @param result_string матрица, в которую будет записываться результат
 * @return код завершения (0)
 */
int multiple_matrix(
	int matrix_string[SIZE_OF_ROWS_AND_COLUMNS * SIZE_OF_ROWS_AND_COLUMNS],
	int result_matrix[SIZE_OF_ROWS_AND_COLUMNS][SIZE_OF_ROWS_AND_COLUMNS]);
