#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main(void)
{
	/* Запуск генератора рандомних чисел */
	time_t t;
	srand((unsigned)time(&t));

	int **matrix = (int **)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(int *));
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		*(matrix + i) =
			(int *)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(int));
	}
	/* Заповнюемо матрицю */
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS; j++) {
			*(*(matrix + i) + j) =
				rand() % 10; // matrix[i][i] = rand() % 10;
		}
	}

	int *diagonal_elements =
		(int *)malloc(NUMBER_OF_COLS_AND_ROWS * sizeof(int));
	/* Заповнюю майбутній масив з елементами діагоналей */
	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		*(diagonal_elements + i) = 0; // diagonal_elements = 0;
	}

	/* Викликаю функцію, що бере елементи з діагоналі матриці й розташовує їх у порядку зростання */
	diagonal_function(matrix, diagonal_elements);

	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
		free(*(matrix + i));
	}
	free(matrix);
	free(diagonal_elements);

	return 0;
}
