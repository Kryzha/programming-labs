# Лабораторна робота №10. Вступ до показчиків


## Мета

Отримати навички розробки програм мовою С, з використанням утиліт: Doxygen, Makefile, check, llvm code coverage, valgrind

## 1 Вимоги

### 1.1 Розробник

Інформація про розробника:

- Крижановський Ілля Миколайович;
- КН-921б;

### 1.2 Загальне завдання

- Додати перевірку втечі памяті за допомогою утиліти valgrind в Makefile

- Зробити одне з поданних завдань у лабораторній роботі.

- При розробці слід дотримуатись таких особливостей:

- > - програма має мати документацію, що оформлена за допомогою утиліти doxygen;
  > - звіт повинен бути оформлений згідно “Вимогам до структурної побудови звіту”;
  > - продемонструвати відсутність витоків пам’яті за допомогою утиліти valgrind;
  > - доступ до елементів масиву здійснювати через розіменування покажчиків, а не через
  >   оператор індексування ([ ]);
  > - продемонструвати роботу розроблених методів за допомогою модульних тестів;
  >   у звіті навести ступень покриття коду модульними тестами. 50% - є мінімально допустимим
  >   відсотком покриття коду тестами.
  > - (як і раніше) забороняється використовувати функції введення/виведення. Вхідні дані
  >   повинні бути у вигляді констант, вихідні дані повинні бути відображені за допомогою
  >   відлагодника.

### 1.3 Задача
- Зробити программу, яка буде брати діагональ з матриці, та сортувати її за порядком зростання

   # Опис програми

  ### 2.1 Функціональне призначення

  Программа призначена для того, щоб закріпити знання з використанням показника у мові программуваня

  ### 2.2 Опис логічної структури

  Проект складається з такої структури:

  ```c
  ├── doc
  │   └── Laba9.md
  ├── Doxyfile
  ├── Makefile
  ├── README.md
  ├── src
  │   ├── lib.c
  │   ├── lib.h
  │   └── main.c
  └── test
      └── test.c
  ```

  ### 2.3 Важливі фрагменти програми

  - Скомпільвати программу можна за допомогою команди `make` => `make all`

  - Зробити тест програми можна за допомогою команди `make test`

  - Зробити текст програми на втечу памяті можна за допомогою команди `make leak-check`

    ```c
    leak-check: distclean format $(MAIN_PATH) $(TEST_PATH)
    	valgrind $(V_FLAGS) --log-file=$(EXE_DIR)/valgrind.log --xml-file=$(EXE_DIR)/valgrind.xml --xml=yes $(EXE_DIR)/$(MAIN_EXE)
    	valgrind $(V_FLAGS) --log-file=$(EXE_DIR)/valgrind.log --xml-file=$(EXE_DIR)/valgrind.xml --xml=yes $(EXE_DIR)/$(TEST_EXE)
    ```

  - Зробити документацію можна за допомогою команди `make docgen`

  - Усі константи задукоментовані, та вложені в файл `lib.h`

  - Оснона суть програми - перебирати масиви за допомогою розменування показчиків:

    ```c
    for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS; i++) {
    		for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS; j++) {
    			if (i == j) {
    				*(diagonal_elements + i) = *(
    					*(matrix + i) +
    					j); // diagonal_elements[i] = matrix[i][j];
    			}
    		}
    	}
    	for (int i = 0; i < NUMBER_OF_COLS_AND_ROWS - 1; i++) {
    		for (int j = 0; j < NUMBER_OF_COLS_AND_ROWS - i - 1; j++) {
    			if (*(diagonal_elements + j) >
    			    *(diagonal_elements + (j + 1))) {
    				int current_element =
    					*(diagonal_elements + (j));
    				*(diagonal_elements + (j)) =
    					*(diagonal_elements + (j + 1));
    				*(diagonal_elements + (j + 1)) =
    					current_element;
    			}
    		}
    	}
    ```

    

> Для того, щоб запустити программу та побачити її роботу, достатьно виповнити bash команду:
>
> `./dist/main.bin` (Перед цим зробити компіляцію за допомогою команди `make`). Для того, щоб відслідити результат виконання программи - зробіть тест, ябо відслідіть за допомогою відлагодника lldb поставивши breakpoint на 43 строку перед цим змінивши змінні (`b 43`).

Корректний результат виконання программи:

```
kryzha@kryzha-VivoBook-ASUSLaptop-X512FJ-X512FJ:~/Рабочий стол/Лабораторки/Programming labs/Laba10$ make
удалён 'lib.o'
удалён 'main.o'
удалён 'test.o'
удалён 'default.profraw'
удалён 'dist/main.bin'
удалён 'dist/test.bin'
удалён каталог 'dist'
clang-format src/* -i
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/lib.c -o lib.o
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/main.c -o main.o
mkdir -p dist
clang -fprofile-instr-generate -fcoverage-mapping lib.o main.o -o dist/main.bin
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping -Isrc test/test.c -o test.o
clang -fprofile-instr-generate -fcoverage-mapping lib.o test.o -lcheck -lm -lrt -lpthread -lsubunit -o dist/test.bin
./dist/main.bin
kryzha@kryzha-VivoBook-ASUSLaptop-X512FJ-X512FJ:~/Рабочий стол/Лабораторки/Programming labs/Laba10$ make test
dist/test.bin
Running suite(s): Programming
100%: Checks: 1, Failures: 0, Errors: 0
test/test.c:31:P:Lab10:diagonal_test:0: Passed
llvm-profdata merge -sparse default.profraw -o default.profdata
llvm-cov report dist/test.bin -instr-profile=default.profdata src/*.c
Filename                                                                                          Regions    Missed Regions     Cover   Functions  Missed Functions  Executed       Lines      Missed Lines     Cover
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/home/kryzha/Рабочий стол/Лабораторки/Programming labs/Laba10/src/lib.c          21                 1    95.24%           1                 0   100.00%          24                 7    70.83%
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TOTAL                                                                                                  21                 1    95.24%           1                 0   100.00%          24                 7    70.83%
llvm-cov show dist/test.bin -instr-profile=default.profdata src/*.c -format html > coverage.html
kryzha@kryzha-VivoBook-ASUSLaptop-X512FJ-X512FJ:~/Рабочий стол/Лабораторки/Programming labs/Laba10$ make leak-check
удалён 'lib.o'
удалён 'main.o'
удалён 'test.o'
удалён 'default.profdata'
удалён 'default.profraw'
удалён 'dist/main.bin'
удалён 'dist/test.bin'
удалён каталог 'dist'
удалён 'coverage.html'
clang-format src/* -i
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/lib.c -o lib.o
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/main.c -o main.o
mkdir -p dist
clang -fprofile-instr-generate -fcoverage-mapping lib.o main.o -o dist/main.bin
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping -Isrc test/test.c -o test.o
clang -fprofile-instr-generate -fcoverage-mapping lib.o test.o -lcheck -lm -lrt -lpthread -lsubunit -o dist/test.bin
valgrind --tool=memcheck --leak-check=full --show-reachable=yes --undef-value-errors=yes --track-origins=no --child-silent-after-fork=no --trace-children=no --error-exitcode=1 --log-file=dist/valgrind.log --xml-file=dist/valgrind.xml --xml=yes dist/main.bin
valgrind --tool=memcheck --leak-check=full --show-reachable=yes --undef-value-errors=yes --track-origins=no --child-silent-after-fork=no --trace-children=no --error-exitcode=1 --log-file=dist/valgrind.log --xml-file=dist/valgrind.xml --xml=yes dist/test.bin
Running suite(s): Programming
100%: Checks: 1, Failures: 0, Errors: 0
test/test.c:31:P:Lab10:diagonal_test:0: Passed
```

## Висновок

На цій лабораторній роботи я навчився робити з показчиками в масивах в мові програмування c й утилітої для відсліжування втечі памяті valgrind.
