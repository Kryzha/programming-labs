#include "lib.h"
#include "entity.h"
#include <check.h>

START_TEST(str_to_digit_test)
{


struct TrainsArrayContainer *trains = init_dynamic_array(3);


get_from_file("./assets/input.txt", trains);


sort_trains(trains);


/* Перевірення результатів */
ck_assert_int_eq((trains->array + 0)->carriage_number, 11);

free(trains);
}
END_TEST

int main()
{
    Suite *s = suite_create("Programming labs");
    TCase *tc_core = tcase_create("Laba14");

    tcase_add_test(tc_core, str_to_digit_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
