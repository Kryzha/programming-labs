#pragma once
#ifndef LIB_H
#define LIB_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "entity.h"

/** Константа, яка відображає, скільки максимум символів може бути розташовано у файлі */
#define BUFER 10000

#define MAX_TYPE 50

/** Структура, що представляє із себе рух потяга */
struct TrainWay {
	char start[MAX_TYPE];
	char stop[MAX_TYPE];
};

/** Структура, яка представляє із потяг */
struct Train {
	int need_to_fix;
	char train_number[MAX_TYPE];
	int carriage_number;
	struct TrainWay trainway;
	char train_type[MAX_TYPE];
};

/** Функція, що бере дані з файлу
 * @param file_name Назва файлу
 * @param structure Структура, в яку буде записуватись результат */
void get_from_file(char *file_name, struct TrainsArrayContainer *container);

/** Функція, що кладе данні в файл
 * @param file_name Назва файлу
 * @param structure Структура, в яка буде записана в файл */
void put_in_file(char *file_name, struct TrainsArrayContainer *container);

/** Функція, для сортування контрольної роботи по кількості теоритичних запитань
 * @param structure структура, яка буде відсортирована
 * @param sorting_number кількість теоритичних питань, по якому буде проходити сортування
 * @return Відсортирована структура */
void sort_trains(struct TrainsArrayContainer *container);

/** Функція, для виводу массиву структур на єкран
 * @param structure Структура, яка буде виведена */
void print_trains(struct TrainsArrayContainer *container);

#endif //LIB_H