#include "lib.h"

void get_from_file(char *file_name, struct TrainsArrayContainer *container)
{
	FILE *file = fopen(file_name, "r");
	char line[BUFER];
	int current_array_element = container->size - 1;
	while ((fscanf(file, "%[^\n]", line)) != EOF) {
		fgetc(file);

		sscanf(line, "{ %d %s %d { %s %s } %s }",
		       &(container->array + current_array_element)->need_to_fix,
		       (container->array + current_array_element)->train_number,
		       &(container->array + current_array_element)
				->carriage_number,
		       (container->array + current_array_element)
			       ->trainway.start,
		       (container->array + current_array_element)->trainway.stop,
		       (container->array + current_array_element)->train_type);

		current_array_element--;
	}
	fclose(file);
}

void put_in_file(char *file_name, struct TrainsArrayContainer *container)
{
	FILE *output_file = fopen(file_name, "a+");
	for (int i = 0; i < container->size; i++) {
		fprintf(output_file, "{ %d %s %d { %s %s } %s }\n",
			(container->array + i)->need_to_fix,
			(container->array + i)->train_number,
			(container->array + i)->carriage_number,
			(container->array + i)->trainway.start,
			(container->array + i)->trainway.stop,
			(container->array + i)->train_type);
	}
	fclose(output_file);
}

void sort_trains(struct TrainsArrayContainer *container)
{
	for (int i = 0; i < container->size; i++) {
		if ((container->array + i)->carriage_number <= 10 &&
		    (container->array + i)->need_to_fix == 0) {
			delete_item(container, i + 1);
			i--;
		}
	}
}

void print_trains(struct TrainsArrayContainer *container)
{
	for (int i = 0; i < container->size; i++) {
		printf("{ %d %s %d { %s %s } %s }\n",
		       (container->array + i)->need_to_fix,
		       (container->array + i)->train_number,
		       (container->array + i)->carriage_number,
		       (container->array + i)->trainway.start,
		       (container->array + i)->trainway.stop,
		       (container->array + i)->train_type);
	}
}
