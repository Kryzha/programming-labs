#pragma once
#ifndef ENTITY_H
#define ENTITY_H
#include "lib.h"

/** Структура, що слугує контейнером для динамічного масиву зі списком потягів */
struct TrainsArrayContainer {
	struct Train *array;
	int size;
};

/** Функція, що стврює масив з вказаною кількістью елементів
 * @param container Контейнер динамічного масиву, який буде заповнено (ініціновано)
 * @param size Кількість елементів, які буде додано (ініційовано) в масиві */
struct TrainsArrayContainer *init_dynamic_array(int size);

/** Функція, що додає елемент в динамічний массив
 * @param container Контейнер динамічного масиву
 * @param position Місце у масиві, в яке буде записан елемент
 * @param element Елемент, що будемо записувати у масив */
void insert(struct TrainsArrayContainer *container, int position,
	    struct Train *element);

/** Функція, що видаля елемент з масиву
 * @param container Контейнер динамічного масиву з якого буде видалено елемент
 * @param position Місце елемента, який буде видалений */
void delete_item(struct TrainsArrayContainer *container, int position);

#endif //ENTITY_H
