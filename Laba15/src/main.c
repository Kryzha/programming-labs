#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main(int argc, char **argv)
{
	/* Створення змінних, що зберігають у собі директорії з вхідним файлом та вихідним файлом */
	char *input_dir = *(argv + 1), *output_dir = *(argv + 2);

	/* Створення потрібних зміних для роботи */
	int trains_array_size = 3; // editable
	struct TrainsArrayContainer *trains_array_container =
		init_dynamic_array(trains_array_size);

	/* Читання елементів з файлу */
	get_from_file(input_dir, trains_array_container);

	/* Функція для роботи з колекцією (Сортування по кількості теоретичних питань) */
	sort_trains(trains_array_container);

	/* Виведення елементів масиву на екран */
	printf("Результат виконання програми: \n");
	print_trains(trains_array_container);

	/* Записування даних в файл */
	put_in_file(output_dir, trains_array_container);

	/* Очищення пам`яті та завершення роботи програми */
	free(trains_array_container);
	return 0;
}
