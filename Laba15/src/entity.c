#include "lib.h"

struct TrainsArrayContainer *init_dynamic_array(int size)
{
	struct TrainsArrayContainer *container =
		malloc(sizeof(struct TrainsArrayContainer));

	container->array = malloc(size * sizeof(struct Train));
	container->size = size;

	return container;
}

void insert(struct TrainsArrayContainer *container, int position,
	    struct Train *element)
{
	struct Train *new_array =
		malloc((container->size + 1) * sizeof(struct Train));

	if (position > container->size) {
		position = container->size;
	}

	memcpy(new_array, container->array, position * sizeof(struct Train));
	memcpy(new_array + position, element, sizeof(struct Train));
	memcpy(new_array + position + 1, container->array + position,
	       (container->size - position) * sizeof(struct Train));

	free(container->array);
	container->array = new_array;
	container->size++;
}

void delete_item(struct TrainsArrayContainer *container, int position)
{
	if (container->size == 0) {
		return;
	}

	struct Train *new_array =
		malloc((container->size - 1) * sizeof(struct Train));

	if (position >= container->size)
		position = container->size - 1;

	memcpy(new_array, container->array, position * sizeof(struct Train));
	memcpy(new_array + position, container->array + position + 1,
	       (container->size - position - 1) * sizeof(struct Train));

	free(container->array);
	container->array = new_array;
	container->size--;
}
