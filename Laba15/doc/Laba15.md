# Лабораторна робота №15. Динамічні масиви

## Мета

Отримати навички розробки програм мовою С, які мають у собі динамічні масиви.

## 1 Вимоги

### 1.1 Розробник

Інформація

- Крижановський Ілля Миколайович;
- КН-921Б;

### 1.2 Загальне завдання

- Переробити лабораторну роботу номер 14 так, щоб ЇЇ робота включала в себе динамічні масиви (замість статичних).

### 1.3 Задача

- Розробити функцію та структури для роботи з динамічними масивами.
- Переробити старі функції для роботи з динамічними масивами.


> Для того, щоб запустити программу та побачити її роботу, достатьно виповнити bash команду:
>
> `./dist/main.bin "./assets/input.txt" "./dist/output.txt"`.
>
> Зробити документацію до проекта можна за допомогую команди `make docgen`.
>
> Зробити модульні тести до ціеї програми можна за допомогою команди `make test`.

В результаті виконання лабораторної роботи були розроблені доповнюючі файлі `entity.c` й `entity.h`, в яких реалузіються функції для роботи з динамічними масивами.

Код для роботи з динамічними масивами (`entity.c`) виглядає наступним чином:

```c
#include "lib.h"

struct TrainsArrayContainer *init_dynamic_array(int size)
{
	struct TrainsArrayContainer *container =
		malloc(sizeof(struct TrainsArrayContainer));

	container->array = malloc(size * sizeof(struct Train));
	container->size = size;

	return container;
}

void insert(struct TrainsArrayContainer *container, int position,
	    struct Train *element)
{
	struct Train *new_array =
		malloc((container->size + 1) * sizeof(struct Train));

	if (position > container->size) {
		position = container->size;
	}

	memcpy(new_array, container->array, position * sizeof(struct Train));
	memcpy(new_array + position, element, sizeof(struct Train));
	memcpy(new_array + position + 1, container->array + position,
	       (container->size - position) * sizeof(struct Train));

	free(container->array);
	container->array = new_array;
	container->size++;
}

void delete_item(struct TrainsArrayContainer *container, int position)
{
	if (container->size == 0) {
		return;
	}

	struct Train *new_array =
		malloc((container->size - 1) * sizeof(struct Train));

	if (position >= container->size)
		position = container->size - 1;

	memcpy(new_array, container->array, position * sizeof(struct Train));
	memcpy(new_array + position, container->array + position + 1,
	       (container->size - position - 1) * sizeof(struct Train));

	free(container->array);
	container->array = new_array;
	container->size--;
}

```

Корректний результат виконання программи:

```c
kryzha@kryzha-VivoBook-ASUSLaptop-X512FJ-X512FJ:~/Рабочий стол/Лабораторки/Programming labs/Laba15$ make 
clang-format src/* -i
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/lib.c -o lib.o
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/entity.c -o entity.o
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping src/main.c -o main.o
mkdir -p dist
clang -fprofile-instr-generate -fcoverage-mapping lib.o entity.o main.o  -o dist/main.bin
clang -c -Og -g -fprofile-instr-generate -fcoverage-mapping -Isrc test/test.c -o test.o
clang -fprofile-instr-generate -fcoverage-mapping lib.o entity.o test.o -lcheck -lm -lrt -lpthread -lsubunit -o dist/test.bin
./dist/main.bin "./assets/input.txt" "./dist/output.txt"
Результат виконання програми: 
{ 1 Baz 11 { Odessa Donetsk } Electric_locomotive }
```

Також після виконання программи ви знайдете файл, у який було відведено результат `dist/output.txt`

## Висновок

На цій лабораторній работі я навчився робити програми з динамічними масивами в мові програмування C.

