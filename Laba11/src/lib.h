#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/**Константа, що описує кількість стовпчиків та колонок у масиві масивів*/
#define NUMBER_OF_COLS_AND_ROWS 3
/**Константа, що описує кільість стовпчиків та колонок у матриці з матиматичного доповнення*/
#define NUMBER_OF_COLS_AND_ROWS_IN_MATH_ADDITION 2

/**Функція заповнення матриці, шляхом запросу данних у користувача
 * @param matrix матриця, в яку будуть вводитися данні
 */
void seed_matrix(double **matrix);

/**Функція виведення матриці у консоль
 * @param matrix матриця, яка буде виводитися у консолі
 */
void print_matrix(double **matrix);

/**Функція розрахунку детермінанта
 * @param matrix матриця, детермінант якої буде розрахован
 * @return детермінант матриці
 */
double matrix_determinant_count(double **matrix);

/**Функція транспонування матриці
 * @param matrix матриця, яка буде транспонована
 * @param result_matrix матриця, в яку буде записан результат
 */
void transpose_matrix(double **matrix, double **result_matrix);

/**Функція розрахунку детермінанта
 * @param matrix матриця, з якої будуть розраховуватися матиматичні доповнення елементу
 * @param row рядок елемента
 * @param col стовпчик елемента
 * @return матиматичне доповнення елементу
 */
double math_addition(double **matrix, int row, int col);

/**Функція розрахунку детермінанта
 * @param matrix матриця, з якої буде створена обернена
 * @param result_matrix матриця, в яку буде записуватись результат
 * @return результат транспонування (була матриця транспонована, чи ні). false у разі помилки
 */
bool reverse_matrix(double **matrix, double **result_matrix);