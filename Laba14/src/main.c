#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main(int argc, char **argv)
{
	/* Створення змінних, що зберігають у собі директорії з вхідним файлом та вихідним файлом */
	char *input_dir = *(argv + 1), *output_dir = *(argv + 2);

	/* Створення потрібних зміних для роботи */
	struct Train *trains = malloc(TRAINS * sizeof(struct Train));

	/* Читання елементів з файлу */
	get_from_file(input_dir, trains);

	/* Функція для роботи з колекцією (Сортування по кількості теоретичних питань) */
	struct Train *sorted_trains = sort_trains(trains);

	/* Виведення елементів масиву на екран */
	printf("Результат виконання програми: \n");
	print_trains(sorted_trains);

	/* Записування даних в файл */
	put_in_file(output_dir, trains);

	/* Очищення пам`яті та завершення роботи програми */
	free(trains);
	free(sorted_trains);
	return 0;
}
