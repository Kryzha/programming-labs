#include "lib.h"

void get_from_file(char *file_name, struct Train *structure)
{
	FILE *file = fopen(file_name, "r");
	char line[BUFER];
	int current_array_element = TRAINS - 1;
	while ((fscanf(file, "%[^\n]", line)) != EOF) {
		fgetc(file);

		sscanf(line, "{ %d %s %d { %s %s } %s }",
		       &(structure + current_array_element)->need_to_fix,
		       (structure + current_array_element)->train_number,
		       &(structure + current_array_element)->carriage_number,
		       (structure + current_array_element)->trainway.start,
		       (structure + current_array_element)->trainway.stop,
		       (structure + current_array_element)->train_type);

		current_array_element--;
	}
	fclose(file);
}

void put_in_file(char *file_name, struct Train *structure)
{
	FILE *output_file = fopen(file_name, "a+");
	for (int i = 0; i < TRAINS; i++) {
		fprintf(output_file, "{ %d %s %d { %s %s } %s }\n",
			(structure + i)->need_to_fix,
			(structure + i)->train_number,
			(structure + i)->carriage_number,
			(structure + i)->trainway.start,
			(structure + i)->trainway.stop,
			(structure + i)->train_type);
	}
	fclose(output_file);
}

struct Train *sort_trains(struct Train *structure)
{
	struct Train *result = malloc(TRAINS * sizeof(struct Train));

	for (int i = 0; i < TRAINS; i++) {
		if ((structure + i)->carriage_number > 10 &&
		    (structure + i)->need_to_fix == 1) {
			*(result + i) = *(structure + i);
		}
	}

	return result;
}

void print_trains(struct Train *structure)
{
	for (int i = 0; i < TRAINS; i++) {
		printf("{ %d %s %d { %s %s } %s }\n",
		       (structure + i)->need_to_fix,
		       (structure + i)->train_number,
		       (structure + i)->carriage_number,
		       (structure + i)->trainway.start,
		       (structure + i)->trainway.stop,
		       (structure + i)->train_type);
	}
}
