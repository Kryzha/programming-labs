#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** Константа, що вказує на кількість елементів в масиві - кількість потягів */
#define TRAINS 3

/** Константа, яка відображає, скільки максимум символів може бути розташовано у файлі */
#define BUFER 10000

#define MAX_TYPE 50

//LOCOMOTIVE, PUFFER, ELECTRIC_LOCOMOTIVE

/** Структура, що представляє із себе рух потяга */
struct TrainWay {
	char start[MAX_TYPE];
	char stop[MAX_TYPE];
};

/** Структура, яка представляє із потяг */
struct Train {
	int need_to_fix;
	char train_number[MAX_TYPE];
	int carriage_number;
	struct TrainWay trainway;
	char train_type[MAX_TYPE];
};

/** Функція, що бере дані з файлу
 * @param file_name Назва файлу
 * @param structure Структура, в яку буде записуватись результат */
void get_from_file(char *file_name, struct Train *structure);

/** Функція, що кладе данні в файл
 * @param file_name Назва файлу
 * @param structure Структура, в яка буде записана в файл */
void put_in_file(char *file_name, struct Train *structure);

/** Функція, для сортування контрольної роботи по кількості теоритичних запитань
 * @param structure структура, яка буде відсортирована
 * @param sorting_number кількість теоритичних питань, по якому буде проходити сортування
 * @return Відсортирована структура */
struct Train *sort_trains(struct Train *structure);

/** Функція, для виводу массиву структур на єкран
 * @param structure Структура, яка буде виведена */
void print_trains(struct Train *structure);

/* Поля базового класу:
– Тип потягу (один з переліку: тепловоз, паровоз, електропотяг)
• Спадкоємець 1 - Пасажирський потяг. Додаткові поля:
– Ціна квитка, USD (наприклад: 10, 15, 20)
– Кількість місць у вагоні (наприклад: 100, 150)
• Спадкоємець 2 - Вантажний потяг. Додаткові поля:
– Тип вантажу (один з переліку: дерево, вугілля, хлор, нафта)
– Маса вагону, т (наприклад: 20, 50)
• Методи для роботи з колекцією:
1. Знайти всі потяги з кількістю вагонів більш ніж 10, які потребують капітального
ремонту
662. Знайти всі вантажні потяги, що прямують з України
3. Знайти вантажний потяг з найбільшою масою, серед тих, що перевозять дерево
*/
