#include "lib.h"
#include <check.h>

START_TEST(str_to_digit_test)
{


struct Train *trains = malloc(TRAINS * sizeof(struct Train));


get_from_file("./assets/input.txt", trains);


struct Train *sorted_trains = sort_trains(trains);


/* Перевірення результатів */
        ck_assert_int_eq((sorted_trains + 0)->carriage_number, 11);


free(trains);
free(sorted_trains);
}
END_TEST

int main()
{
    Suite *s = suite_create("Programming labs");
    TCase *tc_core = tcase_create("Laba14");

    tcase_add_test(tc_core, str_to_digit_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
